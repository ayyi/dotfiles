echo "Sync your dotfiles..."
cp -f  ~/.zshrc ./
cp -rf ~/.vim   ./
cp -f  ~/.vimrc ./
cp -rf /usr/share/icons/infinity-icons ./.icons/
echo "Copied dotfiles!"
git add .
git commit -m "$(date +%d.%m.%y.\ %H:%M:%S)"
git push
echo "Uploaded your data to the git server!"
