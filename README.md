# Dotfiles

Hello! I am glad that you are interested in my dot files. 
You can just download and use them. 
I just use this as a backup and can give others a quick and easy link for the repo if needed.

Information about my PC, you can read from the `neofetch`, which is included in the screenshot.

---
Preview:

![screenshot](./preview/screenshot.png)
![screenshot_2](./preview/screenshot_2.png)
![screenshot_3](./preview/screenshot_3.png)